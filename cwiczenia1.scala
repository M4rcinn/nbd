import scala.annotation.tailrec

object Cwiczenie1 {

  def main(args: Array[String]) = {
     /**1.	Stwórz 7 elementową listę zawierającą nazwy dni tygodnia. Napisz funkcję tworzącą w oparciu o nią stringa z elementami oddzielonymi przecinkami korzystając z:
a.	Pętli for
b.	Pętli for wypisując tylko dni z nazwami zaczynającymi się na „P”
c.	Pętli while
   * 
	*/
    println("Zadanie 1: ")
    pierwsze() // Calling function
    
    /**
   * 2.	Dla listy z ćwiczenia 1 napisz funkcję tworzącą w oparciu o nią stringa z elementami oddzielonymi przecinkami korzystając z:
a.	Funkcji rekurencyjnej 
b.	Funkcji rekurencyjnej wypisując elementy listy od końca
   * 
   * 
   * 
   */
    println("Zadanie 2: ")
    println(drugie(dniTygodnia))
    println(drugieOdwrotnie(dniTygodnia))
    
    /**
   * 3.	Stwórz funkcję korzystającą z rekurencji ogonowej do zwrócenia oddzielonego przecinkami stringa zawierającego elementy listy z ćwiczenia 1
   */
    println("Zadanie 3: ")
    println(trzecie(dniTygodnia))
    
    /**
   * 4.	Dla listy z ćwiczenia 1 napisz funkcję tworzącą w oparciu o nią stringa z elementami oddzielonymi przecinkami korzystając z:
a.	Metody foldl
b.	Metody foldr
c.	Metody foldl wypisując tylko dni z nazwami zaczynającymi się na „P”
   * 
   */
    println("Zadanie 4: ")
    println(czwarteA(dniTygodnia))
    println(czwarteB(dniTygodnia))
    println(czwarteC(dniTygodnia))

    
     /**
   * 5.	Stwórz mapę z nazwami produktów i cenami. Na jej podstawie wygeneruj drugą, z 10% obniżką cen. Wykorzystaj mechanizm mapowania kolekcji. 
   */
  
    println("Zadanie 5: ")
    val mapaZCenami = Map("Pietruszka" -> 100, "Ananas" -> 200, "Marchew" -> 1000)
    println(mapaZCenami.map({case(a,b) => a -> b * 0.9}))
    
    /**
     * 6.	Zdefiniuj funkcję przyjmującą krotkę z 3 wartościami różnych typów i wypisującą je
     */
    println("Zadanie 6: ")
    val mojaKrotka : (String, Int, Double) = ("Napis", 54, 59.6562)
    funkcjaZKrotka(mojaKrotka)
    
    
    /**
     * 7.	Zaprezentuj działanie Option na dowolnym przykładzie (np. mapy, w której wyszukujemy wartości po kluczu)
     */
    println("Zadanie 7: ")
    val cenaPietruszki : Option[Int] = mapaZCenami.get("Pietruszka")
    if (cenaPietruszki.isDefined) {
      println("Pietruszka kosztuje: " + cenaPietruszki.get)
    } else {
      println("Nie znaleziono")
    }
    
    val cenaSliwki : Option[Int] = mapaZCenami.get("Sliwka")
    if (cenaSliwki.isDefined) {
      println("Sliwka kosztuje: " + cenaSliwki.get)
    } else {
      println("Nie znaleziono")
    }
    
    /**
     * 8.	Napisz funkcję usuwającą zera z listy wartości całkowitych (tzn. zwracającą listę elementów mających wartości różne od 0).  Wykorzystaj rekurencję. 
     */
    println("Zadanie 8: ")
    val listaWartosciCalkowitych = List(0, 2, 0, 4, 5, 6)
    println(listaWartosciCalkowitych)
    println(usuwanieZerZListy(listaWartosciCalkowitych))
    
    
    /**
     * 9.	Zdefiniuj funkcję, przyjmującą listę liczb całkowitych i zwracającą wygenerowaną na jej podstawie listę, w której wszystkie liczby zostały zwiększone o 1. Wykorzystaj mechanizm mapowania kolekcji.
     */
    println("Zadanie 9: ")
    println(listaWartosciCalkowitych)
    println(zwiekszanieO1(listaWartosciCalkowitych))
    
    /**
   * 10.	Stwórz funkcję przyjmującą listę liczb rzeczywistych i zwracającą stworzoną na jej podstawie listę zawierającą wartości bezwzględne elementów z oryginalnej listy należących do przedziału <-5,12>. Wykorzystaj filtrowanie.
   */
    println("Zadanie 10: ")
    val liczbyRzeczywiste = List(33.22, 100, 12, -7, -4.7, 1.3, 0, 1000, 0.4)
    println(liczbyRzeczywiste)
    println(wartosciBezwzgledne(liczbyRzeczywiste))
  }

  /**1.	Stwórz 7 elementową listę zawierającą nazwy dni tygodnia. Napisz funkcję tworzącą w oparciu o nią stringa z elementami oddzielonymi przecinkami korzystając z:
a.	Pętli for
b.	Pętli for wypisując tylko dni z nazwami zaczynającymi się na „P”
c.	Pętli while
   * 
	*/
  val dniTygodnia = List("Poniedzialek", "Wtorek", "Sroda", "Czwartek", "Piatek", "Sobota", "Niedziela")

  def pierwsze() = {
    for (dzien <- dniTygodnia)      
      if (dniTygodnia.tail.isEmpty) {
        print(dzien)
      } else {
        print(dzien +", ")
      }
    println()
    for (dzien <- dniTygodnia) 
      if (dzien.startsWith("P"))
        print(dzien +", ")
      
    println()  
    var i =0
    while (i < dniTygodnia.size) {
      if ((i+1) < dniTygodnia.size) {
        print(dniTygodnia(i) +", ");
      } else {
        print(dniTygodnia(i));
      }
      
      i += 1;
    }
    println()  
  }

  
  
  /**
   * 2.	Dla listy z ćwiczenia 1 napisz funkcję tworzącą w oparciu o nią stringa z elementami oddzielonymi przecinkami korzystając z:
a.	Funkcji rekurencyjnej 
b.	Funkcji rekurencyjnej wypisując elementy listy od końca
   * 
   * 
   * 
   */
  def drugie(list: List[String]): String = {
    if (list.isEmpty) return ""
    else {
      return list.head + ", " + drugie(list.tail)
    }
    
  }
  
  def drugieOdwrotnie(list: List[String]): String = {
   if (list.isEmpty) return ""
    else {
      return  list.takeRight(1)(0) + ", " + drugie(list.dropRight(1))
    }
  }
  
  /**
   * pierwotna wersja tego zadania
   * def drugie(list: List[String]): String = {
    @tailrec
    def drugieStworzStringa(list: List[String], zbudowanyString: String): String = {
      list match {
            case Nil => zbudowanyString.drop(2)
            case x :: xs => drugieStworzStringa(xs, zbudowanyString + ", " + x)
        }
    }
    drugieStworzStringa(list, "")
  }
  
  def drugieOdwrotnie(list: List[String]): String = {
    @tailrec
    def drugieStworzStringa(list: List[String], zbudowanyString: String): String = {
      list match {
            case Nil => zbudowanyString.dropRight(2)
            case x :: xs => drugieStworzStringa(xs, x + ", " + zbudowanyString)
        }
    }
    drugieStworzStringa(list, "")
  }
   */
  
  
  /**
   * 3.	Stwórz funkcję korzystającą z rekurencji ogonowej do zwrócenia oddzielonego przecinkami stringa zawierającego elementy listy z ćwiczenia 1
   */
  def trzecie(list: List[String]): String = {
    @tailrec
    def trzecieStworzStringa(list: List[String], zbudowanyString: String): String = {
      list match {
            case Nil => zbudowanyString.drop(2)
            case x :: xs => trzecieStworzStringa(xs, zbudowanyString + ", " + x)
        }
    }
    trzecieStworzStringa(list, "")
  }   
  
  
  /**
   * 4.	Dla listy z ćwiczenia 1 napisz funkcję tworzącą w oparciu o nią stringa z elementami oddzielonymi przecinkami korzystając z:
a.	Metody foldl
b.	Metody foldr
c.	Metody foldl wypisując tylko dni z nazwami zaczynającymi się na „P”
   * 
   */
  def czwarteA(list: List[String]): String = {
   return list.foldLeft("") {(a, b) => a +", "+ b}.drop(2)
  }
    
  def czwarteB(list: List[String]): String = {
   return list.foldRight("") {(a, b) => b +", "+ a}.drop(2)
  }
  
  def czwarteC(list: List[String]): String = {
   return list.foldLeft("") { (a, b) => if (b.startsWith("P")) a + ", " + b else a}.drop(2)
  }
  
 
  /**
     * 6.	Zdefiniuj funkcję przyjmującą krotkę z 3 wartościami różnych typów i wypisującą je
     */
  def funkcjaZKrotka(mojaKrotka : (String, Int, Double)): Unit = println(mojaKrotka._1 + " " + mojaKrotka._2 + " " + mojaKrotka._3)
    
  /**
   * 8.	Napisz funkcję usuwającą zera z listy wartości całkowitych (tzn. zwracającą listę elementów mających wartości różne od 0).  Wykorzystaj rekurencję. 
   */
  def usuwanieZerZListy(lista: List[Int]): List[Int] = {
    return lista.filter(p => p!=0)
  }
  
  /**
   * 9.	Zdefiniuj funkcję, przyjmującą listę liczb całkowitych i zwracającą wygenerowaną na jej podstawie listę, w której wszystkie liczby zostały zwiększone o 1. Wykorzystaj mechanizm mapowania kolekcji.
   */
  def zwiekszanieO1(lista: List[Int]): List[Int] = {
    return lista.map(f => f+1)
  }
  
  /**
   * 10.	Stwórz funkcję przyjmującą listę liczb rzeczywistych i zwracającą stworzoną na jej podstawie listę zawierającą wartości bezwzględne elementów z oryginalnej listy należących do przedziału <-5,12>. Wykorzystaj filtrowanie.
   */
  def wartosciBezwzgledne(lista: List[Double]): List[Double] = {
    return lista.filter(p => p<=12 && p>= -5.0).map(f => (f).abs)
  }
}




