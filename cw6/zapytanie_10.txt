MATCH p = (a1:Airport)<-[r1:ORIGIN]-(f1:Flight)-[r2:DESTINATION]->(a2:Airport)<-[r3:ORIGIN]-(f2:Flight)-[r4:DESTINATION]->(a3:Airport)
WHERE not(a1=a2) AND not(a1=a3) AND not(a2=a3)
WITH p, REDUCE(totalCost = 0, n IN [x IN NODES(p) WHERE 'Flight' IN LABELS(x)] |
  totalCost + [(n)<-[:ASSIGN]-(ticket)|ticket.price][0]
  ) as price
RETURN p, price
ORDER BY price asc LIMIT 1
