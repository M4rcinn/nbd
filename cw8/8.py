import requests

# wrzucenie
print("Wrzucenie dokumentu z polem o wartości '123456'")
r = requests.put('http://localhost:8098/buckets/s14307/keys/dokument', data = {'pole': 123456})
print(r)

# pobranie i wypisanie
print("Pobranie danych i wypisanie")
r = requests.get('http://localhost:8098/buckets/s14307/keys/dokument')
print(r)
print(r.text)

# modyfikacja, pobranie, wypisanie
print("Modyfikacja wartości na '98765', pobranie danych i wypisanie")
r = requests.put('http://localhost:8098/buckets/s14307/keys/dokument', data = {'pole': 98765})
print(r)

r = requests.get('http://localhost:8098/buckets/s14307/keys/dokument')
print(r)
print(r.text)


print("Usunięcie i próba pobrania")
# usunięcie
r = requests.delete('http://localhost:8098/buckets/s14307/keys/dokument')
print(r)
# próba pobrania z bazy
r = requests.delete('http://localhost:8098/buckets/s14307/keys/dokument')

print(r)
print(r.text)

