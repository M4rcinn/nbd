var mapFunction = function() {
    emit(this.nationality, { bmi_sum: this.weight * 10000 / (this.height * this.height),
    bmi_max: this.weight * 10000 / (this.height * this.height),
    bmi_min: this.weight * 10000 / (this.height * this.height),
     liczba: 1 });
};
// liczenie ok, ale struktura taka musi być taka sama w mapFunction i reudceFuntion
var reduceFunction = function(id, bmis) {
    return {bmi_sum: Array.sum(bmis.map(w => w.bmi_sum)), liczba: Array.sum(bmis.map(w => w.liczba)), bmi_max: Math.max.apply(Math, bmis.map(w => w.bmi_max)), bmi_min: Math.min.apply(Math, bmis.map(w => w.bmi_min))};	
    

};
var finalizeFunction = function (key, reducedVal) {
    return {
        avg: reducedVal.bmi_sum/reducedVal.liczba,
        max: reducedVal.bmi_max,
        min: reducedVal.bmi_min
    }
}

printjson(db.people.mapReduce(
			mapFunction,
			reduceFunction,
			{
			out: { inline: 1},
			finalize: finalizeFunction
			
			}
			));
			

			

