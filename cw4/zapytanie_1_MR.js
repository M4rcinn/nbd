
var mapFunction = function() {
   emit(this.sex, { waga: parseFloat(this.weight), liczba: 1});
};			

var reduceFunction = function(sex, weight_double) {
     return {waga: Array.sum(weight_double.map(w => w.waga)), liczba: Array.sum(weight_double.map(w => w.liczba))};
};			

var finalizeFunction = function (key, reducedVal) {
  reducedVal.avg = reducedVal.waga/reducedVal.liczba;
  return reducedVal;
}
			
printjson(db.people.mapReduce(
			mapFunction,
			reduceFunction,
			{
			out: { inline: 1},
			finalize: finalizeFunction
			
			}
			));
			
