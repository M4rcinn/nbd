var mapFunction = function () {
    if (this.sex === "Female", this.nationality === "Poland")
        for (var i = 0; i < this.credit.length; i++) {
            emit(this.credit[i].currency, {balance: parseFloat(this.credit[i].balance), licznik: 1});
        };
}


var reduceFunction = function (id, balances) {

    return { sum: Array.sum(balances.map(b => b.balance)), liczba: Array.sum(balances.map(b => b.licznik)) }
};		

var finalizeFunction = function (key, reducedVal) {
  //reducedVal.avg = reducedVal.sum/reducedVal.liczba;
  
  return {
        avg: reducedVal.sum/reducedVal.liczba,
        sum: reducedVal.sum
    }
}
			
printjson(db.people.mapReduce(
			mapFunction,
			reduceFunction,
			{
			out: { inline: 1},
			finalize: finalizeFunction
			
			}
			));
			
