printjson(db.people.insert (
{
		"sex" : "Male",
		"first_name" : "Marcin",
		"last_name" : "Stempkowicz",
		"job" : "Student",
		"email" : "s14307@pjwstk.edu.pl",
		"location" : {
			"city" : "Warszawa",
			"address" : {
				"streetname" : "Moja ulica",
				"streetnumber" : "31"
			}
		},
		"description" : "nam congue risus semper porta volutpat quam pede lobortis ligula sit amet eleifend pede libero quis",
		"height" : "191",
		"weight" : "66.84",
		"birth_date" : "1986-08-14T03:25:43Z",
		"nationality" : "Poland",
		"credit" : [
			{
				"type" : "jcb",
				"number" : "3553430081173258",
				"currency" : "ISK",
				"balance" : "3645.21"
			}
		]
}
)
)

