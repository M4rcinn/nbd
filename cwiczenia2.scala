object Cwiczenie1 {
  
  def main(args: Array[String]) = {
  /**
   * 1.	Wykorzystaj Pattern Matching w funkcji przyjmującej parametr typu String. Dla stringów odpowiadających nazwom dni tygodnia funkcja ma zwrócić „Praca” i „Weekend” (odpowiednio dla dni roboczych i wolnych), dla pozostałych napisów „Nie ma takiego dnia”.
   */
  
  val dniTygodnia = List("Poniedzialek", "Wtorek", "Sroda", "Czwartek", "Piatek", "Sobota", "Niedziela")
  
  println("Zadanie 1: ")
  println("Czwartek: " + dniRobocze("Czwartek"))
  println("Wtorek: " + dniRobocze("Wtorek"))
  println("Niedziela: " + dniRobocze("Niedziela"))
  println("Sobota: " + dniRobocze("Sobota"))
  println("Maj: " + dniRobocze("Maj"))
  
  /**
   * 2.	Zdefiniuj klasę KontoBankowe z metodami wplata i wyplata oraz własnością stanKonta - własność ma być tylko do odczytu. Klasa powinna udostępniać konstruktor przyjmujący początkowy stan konta oraz drugi, ustawiający początkowy stan konta na 0.
   */
  println("Zadanie 2: ")
  println("Pierwsze konto.")
  var konto = new KontoBankowe
  println(konto)
  konto.wplata(1000)
  println(konto)
  konto.wyplata(100)
  println(konto)
  println("Drugie konto.")
  konto = new KontoBankowe(5000)
  println(konto)
  konto.wplata(1000)
  println(konto)
  konto.wyplata(100)
  println(konto)
  
  /**
   * 3.	Zdefiniuj klasę Osoba z własnościami imie i nazwisko. Stwórz kilka instancji tej klasy. Zdefiniuj funkcję, która przyjmuje obiekt klasy osoba i przy pomocy Pattern Matching wybiera i zwraca napis zawierający przywitanie danej osoby. Zdefiniuj 2-3 różne przywitania dla konkretnych osób (z określonym imionami lub nazwiskami) oraz jedno domyślne. 
   */
  println("Zadanie 3: ")
  println(powitanie(Osoba("Jan", "Kowalski")))
  println(powitanie(Osoba("Tomasz", "Nowak")))
  println(powitanie(Osoba("Michal", "Kowal")))
  
  
  /**
   * 4.	Zdefiniuj funkcję przyjmującą dwa parametry - wartość całkowitą i funkcję operującą na wartości całkowitej. Zastosuj przekazaną jako parametr funkcję trzykrotnie do wartości całkowitej i zwróć wynik.
   */
  println("Zadanie 4: ")
  val liczbaCalkowita =2
  println("Liczba do przekazania: "+liczbaCalkowita)
  println("Wywolanie z potegowaniem: " +funkcjaZad4(liczbaCalkowita, pow2)) // 2*2 =4, 4*4 = 16, 16*16 = 265
  println("Wywolanie z odejmowaniem 1: " +funkcjaZad4(liczbaCalkowita, minus1))
  
  
  /**
   * 5.	Zdefiniuj klasę Osoba i trzy traity: Student, Nauczyciel, Pracownik. Osoba powinna mieć własności read only: imie, nazwisko, podatek. Pracownik powinien mieć własność pensja (z getterem i seterem). Student i Pracownik powinni przesłaniać własność podatek – dla Studenta zwracamy 0, dla Pracownika 20% pensji. Nauczyciel powinien dziedziczyć z Pracownika, dla niego podatek zwraca 10% pensji. Stwórz obiekty z każdym z traitów, pokaż jak podatek działa dla każdego z nich. Stwórz obiekty z traitami Student i Pracownik, pokaż jak podatek zadziała w zależności od kolejności w jakiej te traity zostały dodane przy tworzeniu obiektu. 
   */
  println("Zadanie 5: ")
  println(new OsobaZad5("Jan", "Kowalski", 15))
  println(new OsobaZad5("Marek", "Kowalski", 15) with Student)
  println(new OsobaZad5("Andrzej", "Kowalski", 15) with Pracownik)
  println(new OsobaZad5("Anna", "Nowak", 15) with Nauczyciel)
  println(new OsobaZad5("Student", "Pracownik", 15) with Student with Pracownik)
  println(new OsobaZad5("Pracownik", "Student", 15) with Pracownik with Student)
  }
  
  /**
   * 1.	Wykorzystaj Pattern Matching w funkcji przyjmującej parametr typu String. Dla stringów odpowiadających nazwom dni tygodnia funkcja ma zwrócić „Praca” i „Weekend” (odpowiednio dla dni roboczych i wolnych), dla pozostałych napisów „Nie ma takiego dnia”.
   */
  def dniRobocze(dzien: String): String = dzien match {
    case "Poniedzialek" => "Praca"
    case "Wtorek" => "Praca"
    case "Sroda" => "Praca"
    case "Czwartek" => "Praca"
    case "Piatek" => "Praca"
    case "Sobota" => "Weekend"
    case "Niedziela" => "Weekend"
    case _ => "Nie ma takiego dnia"
  }
  
  class KontoBankowe(poczatkowyStanKonta: Int) {
    def this() = this(0)
    private var _stanKonta: Int = poczatkowyStanKonta
 
    def wplata(kwota : Int): KontoBankowe = {
      this._stanKonta += kwota
      this
    }
 
    def wyplata(kwota : Int): KontoBankowe = {
      this._stanKonta -= kwota
      this
    }
 
    def stanKonta : Int = this._stanKonta
    
    override def toString: String = "Stan konta: "+_stanKonta.toString()
  }
  
  
  case class Osoba(imie: String, nazwisko: String) {
    def nazwa : String = imie + " " + nazwisko
  }
  
  def powitanie(osoba: Osoba): String = osoba match {
    case Osoba("Jan", "Kowalski") => "Witaj, Janku!"
    case Osoba("Tomasz", "Nowak") => "Witaj, Tomku!"
    case _ => "Witaj, "+osoba.imie+"!"
  }
  
  
  
  /**
   * 4.	Zdefiniuj funkcję przyjmującą dwa parametry - wartość całkowitą i funkcję operującą na wartości całkowitej. Zastosuj przekazaną jako parametr funkcję trzykrotnie do wartości całkowitej i zwróć wynik.
   */
  def funkcjaZad4(i: Int, callback: (Int) => Int): Int = {
    
    return callback(callback(callback(i)))
  }
  
  def pow2(v: Int)= v*v
  
  def minus1(v: Int) = v-1
  
  
  /**
   * 5.	Zdefiniuj klasę Osoba i trzy traity: Student, Nauczyciel, Pracownik. Osoba powinna mieć własności read only: imie, nazwisko, podatek. Pracownik powinien mieć własność pensja (z getterem i seterem). Student i Pracownik powinni przesłaniać własność podatek – dla Studenta zwracamy 0, dla Pracownika 20% pensji. Nauczyciel powinien dziedziczyć z Pracownika, dla niego podatek zwraca 10% pensji. Stwórz obiekty z każdym z traitów, pokaż jak podatek działa dla każdego z nich. Stwórz obiekty z traitami Student i Pracownik, pokaż jak podatek zadziała w zależności od kolejności w jakiej te traity zostały dodane przy tworzeniu obiektu. 
   */
  trait Student extends OsobaZad5 {
    private val _podatek = 0
    override def podatek = _podatek
  }
  trait Pracownik extends OsobaZad5{
    private var _pensja = 0
    private val _podatek = 20
    
    def pensja = _pensja
    def pensja_= (v: Int):Unit = _pensja = v 
    override def podatek = _podatek
  }
  trait Nauczyciel extends Pracownik {
    private val _podatek = 10
    override def podatek = _podatek
  }
  class OsobaZad5(im: String, nazw: String, pod: Int) {
    private val _imie = im;
    private val _nazwisko = nazw;
    private val _podatek = pod;
    def podatek = _podatek
    def nazwisko = _nazwisko
    def imie = _imie
    
    
    override def toString: String = "Imie: "+_imie + " | nazwisko: "+nazwisko+" | podatek: "+podatek.toString()
  }
  
}
